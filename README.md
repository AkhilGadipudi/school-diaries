## School Diaries - [Video Demo](https://www.youtube.com/watch?v=kkd5DLo274I)

Android application to be used by pre-school or daycare staff and parents of the students.

* Uses Firebase for authentication, storage, and real-time activity feed.
* Teachers create classes, add students to classes and create various posts like photos, attendance, lunch details, medication
details etc. and tag one or more students in the post. That post will be visible on the activity feed of all the tagged kids.
* Parents and other family members have different UI and features from staff. They add children to their accounts using
unique code provided by teacher to be able to see the child�s activity feed.
* Used broadcast listeners, background services and REST APIs to provide other features
---
### Credits
* Daichi Furiya - [Wasabeef Recycler View Animations](https://github.com/wasabeef/recyclerview-animators)
* Georgi Eftimov - [Geftimov Viewpager Transformers](https://github.com/geftimov/android-viewpager-transformers)